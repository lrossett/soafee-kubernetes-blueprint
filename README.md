# Kubernetes Workload Blueprint

Using Kubernetes YAML files as a specification to define cloud native workloads in a car.

## Overview

Define application workloads in a SOAFEE compliant implementation using
Kuberenetes YAML files.

This blueprint abstracts the actual container runtime for application
developers as long that runtime is able to use a Kubernetes deployment 
YAML file as input.

<div align="center">
    <img
        src="./files/images/edge-architecture-overview.png"
        title="Edge Architecture Overview"
        alt="Edge architecture overview diagram"
    />
</div>


## Capabilities

Using Kubernetes YAML files also allows CI testing for such applications in cloud environments on top of a Kubernetes cluster.


<div align="center">
    <img
        src="./files/images/arch.png"
        title="Blueprint highlevel architecture"
        alt="Blueprint architecture diagram"
    />
</div>

### Deployability

Using a single and well defined format to declare workloads allows several orchestrators, runtimes and platform to use
the same YAML source to deploy the same application or service:

* Use it in CI systems to test code changes in a Kubernetes cluster;
* Deploy using orchestrators and runtimes that support Kubernetes YAML APIs, including cloud digital twins;
* Easily test it locally using a local kubenetes "cluster" such as Minikube or container managers.

### Testability

Using a static and declarative format allows tools to both assert and verify if a workload was properly deployed
by reading the YAML file and comparing it against any runtime that supports sourcing workload defintions from
such YAML files (assuming it knows how to use the runtime API to do so).

### Predictiability

Kubenetes YAML files allows one to defined the amount of resources (CPU, Memory and Storage) that will be used by the
service it is meant to represent, making its resource allocation predictable for orchestrators.

### Extendability

It's still possible to add runtime specific data in a Kubernetes YAML file while leaving the original/upstream defintion untouched
by adding extra [metadata](./https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/) targeting specific implementations, making it an "extendable" specification.

## Maintainers

* Leonardo Rossetti (Red Hat)

## Versions

| Release Date | Release              |
| ------------ | -------------------- |
| 2023 April   | [R1](releases/r1.md) |

